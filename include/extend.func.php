<?php
function litimgurls($imgid=0)
{
    global $lit_imglist,$dsql;
    //获取附加表
    $row = $dsql->GetOne("SELECT c.addtable FROM #@__archives AS a LEFT JOIN #@__channeltype AS c
                                                            ON a.channel=c.id where a.id='$imgid'");
    $addtable = trim($row['addtable']);

    //获取图片附加表imgurls字段内容进行处理
    $row = $dsql->GetOne("Select imgurls From `$addtable` where aid='$imgid'");

    //调用inc_channel_unit.php中ChannelUnit类
    $ChannelUnit = new ChannelUnit(2,$imgid);

    //调用ChannelUnit类中GetlitImgLinks方法处理缩略图
    $lit_imglist = $ChannelUnit->GetlitImgLinks($row['imgurls']);

    //返回结果
    return $lit_imglist;
}

function getImgTag($aid, $template = 1, $imgwith=110, $imgheight=110, $num=999)
{
    global $dsql;
    $imgurls = '';
    $row =$dsql->getone( "Select aid,imgurls From `dede_addonimages` where aid='$aid' ");
    $id=$row['aid'];
    $imgurls= $row['imgurls'];

    $dtp = new DedeTagParse();
    $dtp->LoadSource($imgurls);

    switch ($template) {
        case 1:
            $imgTag = '<div class="pic-wrap swiper-item swiper-slide"><img src="~src~" class="w100 pic-img swiper-item-img" /></div>';
            break;
        case 2:
            $imgTag = '<li class="pic-wrap list-item swiper-list-item swiper-slide"><img src="~src~" alt="" class="pic-img studio-photo-img" /></li>';
            break;
        case 3:
            $imgTag = '<div class="pic-wrap swiper-slide"><img src="~src~" alt="" class="pic-img news-img" /></div>';
            break;

        default:
            break;
    }
    if(is_array($dtp->CTags))
    {
        $i=0;
        foreach($dtp->CTags as $ctag)
             {
                if($i<$num){
                    if($ctag->GetName()=="img")
                     {
                         $bigimg = trim($ctag->GetInnerText());
                         if($ctag->GetAtt('ddimg') != $bigimg && $ctag->GetAtt('ddimg')!='')
                         {
                            $litimg = $ctag->GetAtt('ddimg');
                         }
                         $title = $ctag->GetAtt('text');
                         $_imgTag = str_replace('~src~', $bigimg, $imgTag);
                         $imglist .= $_imgTag;
                         $i++;
                     }
                }
            }
        }
    return $imglist;

}

function getImgSrc($aid, $imgwith=110, $imgheight=110, $num=999)
{
    global $dsql;
    $imgurls = '';
    $row =$dsql->getone( "Select aid,imgurls From `dede_addonimages` where aid='$aid' ");
    $id=$row['aid'];
    $imgurls= $row['imgurls'];
    $imglist = [];

    $dtp = new DedeTagParse();
    $dtp->LoadSource($imgurls);
    if(is_array($dtp->CTags)) {
        $i=0;
        foreach($dtp->CTags as $ctag)
             {
                if($i<$num){
                    if($ctag->GetName()=="img")
                     {
                         $bigimg = trim($ctag->GetInnerText());
                         array_push($imglist, $bigimg);
                         $i++;
                     }
                }
            }
        }
    return json_encode($imglist);

}

function getMultiLevelColumn($tid, $lang = 'cn')
{

    global $dsql;
    $item1 = $dsql->getone( "Select id, typename, typedir, channeltype From `dede_arctype` where reid='$tid' order by `sortrank` asc limit 0,1");

    $tid1 = $item1['id'];
    $dir1 = $item1['typedir'];
    $dir1 = str_replace('{cmspath}', '', $dir1);
    $channeltype1 = 'dede_archives'; // $item1['channeltype'] > 1 ? 'dede_addonarticle' . $item1['channeltype'] : 'dede_addonarticle';
    $count1 = $dsql->getone("Select count(id) From `$channeltype1` where (typeid = '$tid1' OR typeid2 LIKE '%$tid1%') AND arcrank > -2 ");
    // var_dump($dsql);


    $item2 = $dsql->getone( "Select id, typename, typedir, channeltype From `dede_arctype` where reid='$tid' order by `sortrank` asc Limit 1,1");

    $tid2 = $item2['id'];
    $dir2 = $item2['typedir'];
    $dir2 = str_replace('{cmspath}', '', $dir2);
    $channeltype2 = 'dede_archives'; // $item2['channeltype'] > 1 ? 'dede_addonarticle' . $item2['channeltype'] : 'dede_addonarticle';
    $count2 = $dsql->getone("Select count(id) From `$channeltype2` where (typeid = '$tid2' OR typeid2 LIKE '%$tid2%') AND arcrank > -2 ");
    // var_dump($dsql);

    $count1 = $count1['count(id)'] ? $count1['count(id)'] : 0;
    $count2 = $count2['count(id)'] ? $count2['count(id)'] : 0;


    $btnStr = '';

    if ($lang == 'cn') {
        $tips1 = '可售';
        $tips2 = '成功案例';
        $tips3 = '架';
    } else {
        // $tips1 = 'Available-for-sale airplanes';
        // $tips2 = 'Successful case';
        $tips1 = 'FOR-SALE';
        $tips2 = 'CASES';
        $tips3 = '';
    }

    // $btnStr .= '<a href="/plus/list.php?tid='. $tid1 .'" class="btn-item">';
    $btnStr .= '<a href="'. $dir1 .'" class="btn-item">';
    $btnStr .= '<span class="btn-title">' . $tips1 . '</span>';
    $btnStr .= '<span class="btn-title cur ml-05 mr-05">' . $count1 . '</span>';
    $btnStr .= '<span class="btn-title">' . $tips3 . '</span>';
    $btnStr .= '<img src="/templets/plane/icon/icon_plane.png" class="btn-icon" />';
    $btnStr .= '<img src="/templets/plane/icon/icon_plane1.png" class="btn-icon1" />';
    $btnStr .= '</a>';

    // $btnStr .= '<a href="/plus/list.php?tid='. $tid2 .'" class="btn-item">';
    $btnStr .= '<a href="'. $dir2 .'" class="btn-item">';
    $btnStr .= '<span class="btn-title">' . $tips2 . '</span>';
    $btnStr .= '<span class="btn-title cur ml-05 mr-05">' . $count2 . '</span>';
    $btnStr .= '<span class="btn-title">' . $tips3 . '</span>';
    $btnStr .= '<img src="/templets/plane/icon/icon_plane.png" class="btn-icon" />';
    $btnStr .= '<img src="/templets/plane/icon/icon_plane1.png" class="btn-icon1" />';
    $btnStr .= '</a>';

    return $btnStr;

}